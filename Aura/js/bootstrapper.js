﻿/// <reference path="lib/require.js" />
/// <reference path="events.js" />
/// <reference path="lib/jquery-1.8.3.js" />

require(['jquery', 'events', 'util/logger'], function ( jQuery, events, logger) {
    logger.log("bootstrapper loaded", "loading", "info");

    jQuery(document).ready(function () {
        events.whenReady();
        events.initAfterLoading();
    });

});