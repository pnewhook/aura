﻿/// <reference path="../lib/require.js" />
/* Copyright 2010-2011 Research In Motion Limited.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * An Object to keep track of weather conditions for various dates, gathered for a specific location at a specific point in time.
 * 
 * @param geoLocation The location associated with the gathered weather conditions.
 * @param timeStamp The time at which we are gathering the weather conditions.
 * 
 * @returns {WeatherForecastObj} The Object that holds an empty dates Array intended for specific weather conditions.
 */


define(['util/logger'], function (logger) {
    logger.log("classes loaded", "loading", "info");
    return {
        WeatherForecastObj: function (geoLocation, timeStamp) {
            this.geoLocation = geoLocation;
            this.timeStamp = timeStamp;
            this.dates = [];
        },
        /**
     * An object to keep track of the hourly weather conditions on a given date.
     * 
     * @param day The textual representation of our date (i.e. "Monday", "Tuesday", etc.)
     * @param date The date associated with this Object.
     * 
     * @returns {DateObj} The Object that holds an empty weatherHour array for this date.
     */
        DateObj: function (day, date) {
            this.day = day;
            this.date = date;
            this.weatherHour = [];
        },
        /**
     * An Object to keep track of a temperature:condition pair for a specific weather hour.
     * 
     * @param temperature The temperature, in degrees, at this hour.
     * @param type The weather conditions at this hour (i.e. 'fine', 'cloudy', 'rainy', 'thundery'.)
     * 
     * @returns {WeatherHourObj} The Object that describes this hour's temperature and weather conditions.
     */
        WeatherHourObj: function (temperature, type) {
            this.temperature = temperature;
            this.type = type;
        }
    };
});