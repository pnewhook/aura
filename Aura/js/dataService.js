﻿/// <reference path="lib/require.js" />
/// <reference path="//Microsoft.WinJS.1.0/js/base.js" />

define(['classes', 'util/logger'], function myfunction(classes, logger) {
    "use strict";

    logger.log("dataService loaded", "loading", "info");

    var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    return {
        weatherForecast: null,
        createWeatherForecast: function() {
            var todaysDate = (new Date());
            var dayCounter = todaysDate.getDay();

            var weatherForecast = new classes.WeatherForecastObj(["Spain", "Barcelona"], todaysDate);
            for (var i = 0; i < 4; ++i) {
                weatherForecast.dates[i] = new classes.DateObj(dayNames[dayCounter % dayNames.length], todaysDate.getDate() + i);
                ++dayCounter;

                switch (i) {
                case 0:
                    weatherForecast.dates[i].weatherHour[0] = new classes.WeatherHourObj(8, 'fine');
                    weatherForecast.dates[i].weatherHour[1] = new classes.WeatherHourObj(8, 'fine');
                    weatherForecast.dates[i].weatherHour[2] = new classes.WeatherHourObj(8, 'rainy');
                    weatherForecast.dates[i].weatherHour[3] = new classes.WeatherHourObj(7, 'rainy');
                    weatherForecast.dates[i].weatherHour[4] = new classes.WeatherHourObj(7, 'cloudy');
                    weatherForecast.dates[i].weatherHour[5] = new classes.WeatherHourObj(7, 'cloudy');
                    weatherForecast.dates[i].weatherHour[6] = new classes.WeatherHourObj(9, 'rainy');
                    weatherForecast.dates[i].weatherHour[7] = new classes.WeatherHourObj(9, 'fine');
                    weatherForecast.dates[i].weatherHour[8] = new classes.WeatherHourObj(10, 'rainy');
                    weatherForecast.dates[i].weatherHour[9] = new classes.WeatherHourObj(10, 'rainy');
                    weatherForecast.dates[i].weatherHour[10] = new classes.WeatherHourObj(10, 'thundery');
                    weatherForecast.dates[i].weatherHour[11] = new classes.WeatherHourObj(12, 'thundery');
                    weatherForecast.dates[i].weatherHour[12] = new classes.WeatherHourObj(12, 'cloudy');
                    weatherForecast.dates[i].weatherHour[13] = new classes.WeatherHourObj(13, 'cloudy');
                    weatherForecast.dates[i].weatherHour[14] = new classes.WeatherHourObj(14, 'rainy');
                    weatherForecast.dates[i].weatherHour[15] = new classes.WeatherHourObj(15, 'rainy');
                    weatherForecast.dates[i].weatherHour[16] = new classes.WeatherHourObj(17, 'thundery');
                    weatherForecast.dates[i].weatherHour[17] = new classes.WeatherHourObj(14, 'thundery');
                    weatherForecast.dates[i].weatherHour[18] = new classes.WeatherHourObj(12, 'fine');
                    weatherForecast.dates[i].weatherHour[19] = new classes.WeatherHourObj(12, 'rainy');
                    weatherForecast.dates[i].weatherHour[20] = new classes.WeatherHourObj(10, 'cloudy');
                    weatherForecast.dates[i].weatherHour[21] = new classes.WeatherHourObj(10, 'cloudy');
                    weatherForecast.dates[i].weatherHour[22] = new classes.WeatherHourObj(8, 'fine');
                    weatherForecast.dates[i].weatherHour[23] = new classes.WeatherHourObj(6, 'rainy');
                    break;
                case 1:
                    weatherForecast.dates[i].weatherHour[0] = new classes.WeatherHourObj(8, 'cloudy');
                    weatherForecast.dates[i].weatherHour[1] = new classes.WeatherHourObj(8, 'fine');
                    weatherForecast.dates[i].weatherHour[2] = new classes.WeatherHourObj(8, 'fine');
                    weatherForecast.dates[i].weatherHour[3] = new classes.WeatherHourObj(7, 'rainy');
                    weatherForecast.dates[i].weatherHour[4] = new classes.WeatherHourObj(7, 'cloudy');
                    weatherForecast.dates[i].weatherHour[5] = new classes.WeatherHourObj(7, 'fine');
                    weatherForecast.dates[i].weatherHour[6] = new classes.WeatherHourObj(9, 'rainy');
                    weatherForecast.dates[i].weatherHour[7] = new classes.WeatherHourObj(9, 'cloudy');
                    weatherForecast.dates[i].weatherHour[8] = new classes.WeatherHourObj(10, 'rainy');
                    weatherForecast.dates[i].weatherHour[9] = new classes.WeatherHourObj(10, 'cloudy');
                    weatherForecast.dates[i].weatherHour[10] = new classes.WeatherHourObj(10, 'rainy');
                    weatherForecast.dates[i].weatherHour[11] = new classes.WeatherHourObj(12, 'rainy');
                    weatherForecast.dates[i].weatherHour[12] = new classes.WeatherHourObj(12, 'thundery');
                    weatherForecast.dates[i].weatherHour[13] = new classes.WeatherHourObj(13, 'rainy');
                    weatherForecast.dates[i].weatherHour[14] = new classes.WeatherHourObj(14, 'cloudy');
                    weatherForecast.dates[i].weatherHour[15] = new classes.WeatherHourObj(15, 'fine');
                    weatherForecast.dates[i].weatherHour[16] = new classes.WeatherHourObj(17, 'rainy');
                    weatherForecast.dates[i].weatherHour[17] = new classes.WeatherHourObj(14, 'thundery');
                    weatherForecast.dates[i].weatherHour[18] = new classes.WeatherHourObj(12, 'rainy');
                    weatherForecast.dates[i].weatherHour[19] = new classes.WeatherHourObj(12, 'fine');
                    weatherForecast.dates[i].weatherHour[20] = new classes.WeatherHourObj(10, 'rainy');
                    weatherForecast.dates[i].weatherHour[21] = new classes.WeatherHourObj(10, 'fine');
                    weatherForecast.dates[i].weatherHour[22] = new classes.WeatherHourObj(8, 'fine');
                    weatherForecast.dates[i].weatherHour[23] = new classes.WeatherHourObj(6, 'cloudy');
                    break;
                case 2:
                    weatherForecast.dates[i].weatherHour[0] = new classes.WeatherHourObj(18, 'fine');
                    weatherForecast.dates[i].weatherHour[1] = new classes.WeatherHourObj(18, 'cloudy');
                    weatherForecast.dates[i].weatherHour[2] = new classes.WeatherHourObj(18, 'fine');
                    weatherForecast.dates[i].weatherHour[3] = new classes.WeatherHourObj(17, 'fine');
                    weatherForecast.dates[i].weatherHour[4] = new classes.WeatherHourObj(17, 'cloudy');
                    weatherForecast.dates[i].weatherHour[5] = new classes.WeatherHourObj(17, 'rainy');
                    weatherForecast.dates[i].weatherHour[6] = new classes.WeatherHourObj(19, 'rainy');
                    weatherForecast.dates[i].weatherHour[7] = new classes.WeatherHourObj(19, 'thundery');
                    weatherForecast.dates[i].weatherHour[8] = new classes.WeatherHourObj(20, 'cloudy');
                    weatherForecast.dates[i].weatherHour[9] = new classes.WeatherHourObj(20, 'rainy');
                    weatherForecast.dates[i].weatherHour[10] = new classes.WeatherHourObj(20, 'cloudy');
                    weatherForecast.dates[i].weatherHour[11] = new classes.WeatherHourObj(22, 'rainy');
                    weatherForecast.dates[i].weatherHour[12] = new classes.WeatherHourObj(22, 'rainy');
                    weatherForecast.dates[i].weatherHour[13] = new classes.WeatherHourObj(23, 'thundery');
                    weatherForecast.dates[i].weatherHour[14] = new classes.WeatherHourObj(24, 'cloudy');
                    weatherForecast.dates[i].weatherHour[15] = new classes.WeatherHourObj(25, 'fine');
                    weatherForecast.dates[i].weatherHour[16] = new classes.WeatherHourObj(27, 'thundery');
                    weatherForecast.dates[i].weatherHour[17] = new classes.WeatherHourObj(24, 'cloudy');
                    weatherForecast.dates[i].weatherHour[18] = new classes.WeatherHourObj(22, 'cloudy');
                    weatherForecast.dates[i].weatherHour[19] = new classes.WeatherHourObj(22, 'rainy');
                    weatherForecast.dates[i].weatherHour[20] = new classes.WeatherHourObj(20, 'fine');
                    weatherForecast.dates[i].weatherHour[21] = new classes.WeatherHourObj(20, 'cloudy');
                    weatherForecast.dates[i].weatherHour[22] = new classes.WeatherHourObj(18, 'fine');
                    weatherForecast.dates[i].weatherHour[23] = new classes.WeatherHourObj(16, 'fine');
                    break;
                case 3:
                    weatherForecast.dates[i].weatherHour[0] = new classes.WeatherHourObj(8, 'fine');
                    weatherForecast.dates[i].weatherHour[1] = new classes.WeatherHourObj(8, 'cloudy');
                    weatherForecast.dates[i].weatherHour[2] = new classes.WeatherHourObj(8, 'fine');
                    weatherForecast.dates[i].weatherHour[3] = new classes.WeatherHourObj(7, 'fine');
                    weatherForecast.dates[i].weatherHour[4] = new classes.WeatherHourObj(7, 'thundery');
                    weatherForecast.dates[i].weatherHour[5] = new classes.WeatherHourObj(7, 'fine');
                    weatherForecast.dates[i].weatherHour[6] = new classes.WeatherHourObj(9, 'cloudy');
                    weatherForecast.dates[i].weatherHour[7] = new classes.WeatherHourObj(9, 'rainy');
                    weatherForecast.dates[i].weatherHour[8] = new classes.WeatherHourObj(11, 'cloudy');
                    weatherForecast.dates[i].weatherHour[9] = new classes.WeatherHourObj(11, 'thundery');
                    weatherForecast.dates[i].weatherHour[10] = new classes.WeatherHourObj(12, 'thundery');
                    weatherForecast.dates[i].weatherHour[11] = new classes.WeatherHourObj(13, 'rainy');
                    weatherForecast.dates[i].weatherHour[12] = new classes.WeatherHourObj(14, 'thundery');
                    weatherForecast.dates[i].weatherHour[13] = new classes.WeatherHourObj(15, 'rainy');
                    weatherForecast.dates[i].weatherHour[14] = new classes.WeatherHourObj(16, 'cloudy');
                    weatherForecast.dates[i].weatherHour[15] = new classes.WeatherHourObj(15, 'fine');
                    weatherForecast.dates[i].weatherHour[16] = new classes.WeatherHourObj(17, 'rainy');
                    weatherForecast.dates[i].weatherHour[17] = new classes.WeatherHourObj(14, 'fine');
                    weatherForecast.dates[i].weatherHour[18] = new classes.WeatherHourObj(12, 'rainy');
                    weatherForecast.dates[i].weatherHour[19] = new classes.WeatherHourObj(12, 'cloudy');
                    weatherForecast.dates[i].weatherHour[20] = new classes.WeatherHourObj(10, 'rainy');
                    weatherForecast.dates[i].weatherHour[21] = new classes.WeatherHourObj(10, 'fine');
                    weatherForecast.dates[i].weatherHour[22] = new classes.WeatherHourObj(8, 'rainy');
                    weatherForecast.dates[i].weatherHour[23] = new classes.WeatherHourObj(6, 'fine');
                    break;
                }
            }
            return weatherForecast;
        },
        getCurrentWeather: function(d, a) {

            //Angle to time calculation.
            var convertAngleToHour = Math.ceil(Math.abs(a) / 15) - 1;

            //Map input day and angle to weather Data.
            var currentTemperature = weatherForecast.dates[d].weatherHour[convertAngleToHour].temperature;
            var currentWeatherTyp = weatherForecast.dates[d].weatherHour[d].type;

            return [currentTemperature, currentWeatherTyp];
        },
        mode: function(array) {

            if (array.length === 0) {
                return null;
            }

            var modeMap = [];
            var maxEl = array[0];
            var maxCount = 1;

            for (var i = 0; i < array.length; ++i) {
                var el = array[i];

                // '==' required as '===' will not evaluate properly.
                if (modeMap[el] == null) {
                    modeMap[el] = 1;
                } else {
                    ++modeMap[el];
                }

                if (modeMap[el] > maxCount) {
                    maxEl = el;
                    maxCount = modeMap[el];
                }
            }

            return maxEl;
        },
        getForecastAverage: function() {
            var forecastArray = [];

            for (var j = 0; j < 4; ++j) {
                var averageday = [];
                var tempA = 0;
                var weatherTypeArray = [];

                for (var i = 0; i < 24; ++i) {
                    tempA = tempA + weatherForecast.dates[j].weatherHour[i].temperature;
                    weatherTypeArray[i] = weatherForecast.dates[j].weatherHour[i].type;
                }

                //Adding day average temp and weather type.
                averageday[0] = Math.round(tempA / 24);
                averageday[1] = mode(weatherTypeArray);
                averageday[2] = weatherForecast.dates[j].day;
                forecastArray[j] = averageday;
            }
            return forecastArray;
        }
    };
});
