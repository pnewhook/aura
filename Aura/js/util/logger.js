﻿/// <reference path="../lib/require.js" />
/// <reference path="//Microsoft.WinJS.1.0/js/base.js" />

define(function () {
    "use strict";

    // TODO: use an actual wrapper
    var log;
    if (typeof (WinJS) !== "undefined") {
        WinJS.Utilities.startLog({ type: "info warn error" });
        if (WinJS.log) {
            log = WinJS.log;
        }
    } else {
        log = console.log;
    }

    return {
        log: log
    }
});